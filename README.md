# Covid-19-Stats

Flask based API server for covid-19 statistics, tested on python v3.8.6.

Integrated data from [Johns Hopkins University](https://coronavirus.jhu.edu/) served by [disease.sh](https://disease.sh/) .


## Installation

Install with pip:

```
$ pip3 install -r requirements.txt
```

## Application Structure 
```
|──────app.py
|──────Jenkinsfile
|──────requirements.txt
```


## Run the service
### Run service for develop
```
$ python webapp/run.py
```
Default port : `8080`

Default host : `localhost`

## API endpoints

- newCasesPeak?country=[country] - Returns the date (and value) of the highest peak of new
Covid-19 cases in the last 30 days for a required country.

- recoveredPeak?country=[country]  - Returns the date (and value) of the highest peak of recovered
Covid-19 cases in the last 30 days for the required country.

- deathsPeak?country=[country] - Returns the date (and value) of the highest peak of death Covid-19
cases in the last 30 days for a required country.

- status - Returns a value of success / fail to contact the backend API

## Jenkins
### pipeline Configuration

1. Install the GitHub plugin.
2. Create a new pipeline project.
3. Set the GitHub project url to - https://gitlab.com/itamarlevin94/covid-19-stats.git/
4. Configure pipeline script from SCM, url - https://gitlab.com/itamarlevin94/covid-19-stats.git/
5. Save & Build.
6. After the initial build, insert comma separated `COUNTRIES` parameter. for example : _china,israel,france_. 

### Build Artifacts
- failed_requests_[BUILD_NUMBER].txt - list of failed requests.
- logs_[BUILD_NUMBER].txt - flask service logs.