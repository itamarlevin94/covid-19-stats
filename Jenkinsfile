def BUILD_FOLDER

pipeline {
    agent { docker { image 'python:3.8.6' } }
    parameters {
        string(defaultValue: "china,israel,france", description: 'Input countries list', name: 'COUNTRIES')
    }
    stages {
        stage('Initialize variables') {
            steps{
                script{
                    BUILD_FOLDER="build-${BUILD_NUMBER}"
                }
            }                
        }
        stage('Build') {
            steps {
                withEnv(["HOME=${env.WORKSPACE}"]){
                    sh 'pip3 install -r requirements.txt --user'}
                }
            }
        stage('Test') {
            steps {
                withEnv([
                    "HOME=${env.WORKSPACE}",
                    "FLASK_DEBUG=False",
                    "FLASK_ENV=development",
                    "APP_LOGS_FILE=${BUILD_FOLDER}/logs_${env.BUILD_NUMBER}.txt",
                    "FAILED_REQUESTS_FILE=${BUILD_FOLDER}/failed_requests_${env.BUILD_NUMBER}.txt",
                    "BUILD_FOLDER=${BUILD_FOLDER}"
                    ])
                    {sh '''
                    # remove in case of failure
                    set +x

                    mkdir ${BUILD_FOLDER}

                    nohup python3 app.py > $APP_LOGS_FILE 2>&1 &
                    
                    
                    echo "\nWaiting for service..."
                    until $(curl --output /dev/null --silent --head --fail localhost:8080/status); do
                        echo ...
                        sleep 1
                    done
                    
                    echo Service is up!
                                        
                    echo "\nTesting service:"
                    
                    success=0
                    failure=0

                    for country in $(echo $COUNTRIES | sed "s/,/ /g")
                    do
                        for method in newCasesPeak recoveredPeak deathsPeak
                        do
                            url="localhost:8080/$method?country=$country"
                            echo $url
                            
                            response=`curl -s localhost:8080/$method?country=$country`
                            echo "$response\n"

                            if [ $response = {} ]
                            then
                                failure=$((failure+1))
                                echo $url >> $FAILED_REQUESTS_FILE
                            else
                                success=$((success+1))
                            fi
                        done
                    done
                    
                    echo "\n\n$success Success"
                    echo $failure failure
                    
                    if [ "$failure" -gt 0 ]; then
                        echo "\nFailed requests:"
                        cat $FAILED_REQUESTS_FILE
                        exit 1
                    fi

                    set -x
                    ''' 
                }   
            } 
            post{
                always{
                    archiveArtifacts artifacts: "${BUILD_FOLDER}/*"
                    dir(BUILD_FOLDER) {
                        deleteDir()
                    }
                }
            }
        }
    }
}