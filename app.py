from flask import Flask
from flask import jsonify
from flask import request
import requests
from datetime import datetime, timedelta
from http import HTTPStatus

app = Flask(__name__)

@app.route('/recoveredPeak', methods=['GET'])
@app.route('/deathsPeak', methods=['GET'])
@app.route('/newCasesPeak', methods=['GET'])
def serve_api_data():

    method_switcher = {
        'newCasesPeak': 'cases',
        'recoveredPeak': 'recovered',
        'deathsPeak': 'deaths'
    }

    req_method = request.path.split('/')[1]
    method = method_switcher[req_method]

    country = request.args.get('country', type=str)

    if not country:
        print('ERROR - "country" argument missing')
        return jsonify({}), HTTPStatus.BAD_REQUEST

    http_status_code, stats = get_covid_data_jhu_api(
        method=method, country=country, last_days=30)

    if not stats:
        return jsonify({}), http_status_code

    max_dates, max_value = find_peak_in_timeline(stats)

    return jsonify({
        'country': country,
        'method': req_method,
        'dates': max_dates,
        'value': max_value
    }), HTTPStatus.OK


@app.route('/status')
def status():

    try:
        response = requests.get(
            'https://disease.sh/v3/covid-19/historical/israel?lastdays=1')
    except requests.exceptions.RequestException as e:
        print(f'ERROR - status request error : {e}')
        return jsonify({'status': 'fail'}), HTTPStatus.INTERNAL_SERVER_ERROR

    if response.status_code == HTTPStatus.OK:
        return jsonify({'status': 'success'}), HTTPStatus.OK
    else:
        return jsonify({'status': 'fail'}), response.status_code


# handles all other routes
@app.errorhandler(404)
def handle_404(e):

    return jsonify({}), HTTPStatus.NOT_FOUND


# calculates dates of max values
def find_peak_in_timeline(stats: dict) -> (list, int):

    # reformat dict keys

    stats = {datetime.strptime(date, '%m/%d/%y'): stats[date] for date in stats.keys()}

    # calculate daily value change

    dates_minus_last = list(stats.keys())[1:]

    stats = {date: stats[date] - stats[date - timedelta(days=1)] for date in dates_minus_last}

    # get max value and key of the stat

    max_value = max(stats.values())

    max_dates = [date.strftime('%-m/%-d/%-y') for date, v in stats.items() if v == max_value]

    return (max_dates, max_value)


# documetation for the Johns Hopkins University covid-19 API - https://disease.sh/docs/#/COVID-19%3A%20JHUCSSE
def get_covid_data_jhu_api(method: str, country: str, last_days: int) -> (HTTPStatus, dict):

    url = f'https://disease.sh/v3/covid-19/historical/{country}?lastdays={last_days+1}'

    # Query api

    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as e:
        print(f'ERROR - request failed, URL - {url}, message : {e}')
        return jsonify({'status': 'fail'}), HTTPStatus.INTERNAL_SERVER_ERROR

    if response.status_code != HTTPStatus.OK:
        print(f'ERROR - country={country} response code={response.status_code},response={response.text}')
        return response.status_code, None

    # Process response data

    timeline_stats = response.json()['timeline']

    valid_methods = timeline_stats.keys()

    if method not in valid_methods:
        print(f'{method} is not a valid method, valid methods - {valid_methods}')
        return HTTPStatus.INTERNAL_SERVER_ERROR, None

    return (HTTPStatus.OK, timeline_stats[method])


if __name__ == '__main__':
    app.run(host='localhost', port=8080)
